*** Keywords ***
Add two numbers
    [Arguments]    ${num1}    ${num2}
    ${sum}    Evaluate    ${num1} + ${num2}
    Return From Keyword    ${sum}
    [Return]    ${sum}

Subtract two numbers
    [Arguments]    ${num1}    ${num2}
    ${sub}    Evaluate    ${num1} - ${num2}
    Return From Keyword    ${sub}
    [Return]    ${sub}

Mul
    [Arguments]    ${num1}    ${num2}
    ${mulresult}    Evaluate    ${num1} * ${num2}
    Return From Keyword    ${mulresult}
    [Return]    ${mulresult}

Division
    [Arguments]    ${num1}    ${num2}
    ${divresult}    Evaluate    ${num1} / ${num2}
    Return From Keyword    ${divresult}
    [Return]    ${divresult}

Perform Arithmetic Operation
    [Arguments]    ${operation_type}    ${arg1}    ${arg2}
    Run Keyword If    ${arg1}<0    Fail    Arg1 should be greater than zero but found ${arg1}
    ${result}    Run Keyword If    '${operation_type}'=='Add'    Add two numbers    ${arg1}    ${arg2}
    Run Keyword If    '${operation_type}'=='Add'    Log    ${result}
    ${result}    Run Keyword If    '${operation_type}'=='Substract'    Subtract two numbers    ${arg1}    ${arg2}
    Run Keyword If    '${operation_type}'=='Substract'    Log    ${result}
