*** Settings ***
Resource          ../Global/super.robot

*** Keywords ***
Open Browser and Maximize
    Open Browser    https://www.flipkart.com/    ${BROWSER}
    Maximize Browser Window

Login to Salesforse
    [Arguments]    ${username}    ${password}
    Input Text    username    ${username}
    Input Text    password    ${password}
    Click Element    name:Login

Open Salesforce Application
    Open Browser    https://login.salesforce.com    ${BROWSER}
    Maximize Browser Window
