*** Settings ***
Library           SeleniumLibrary

*** Variables ***
@{cities}
${CITY}           ${EMPTY}

*** Test Cases ***
TC 01 Book Ticket
    Open Browser    https://www.facebook.com/    chrome
    Sleep    2
    Maximize Browser Window
    Sleep    2

TC 02 Textbox
    Open Browser    https://www.facebook.com/    chrome
    Maximize Browser Window
    Wait Until Element Is Visible    //input[@id='email']    2ms    Email field is not visible after waiting 2ms
    Page Should Contain Textfield    xpath://input[@id='email']    It has failed    Warn
    Wait Until Element Is Visible    //input[@id='email']    10s    Email field is not visible after waiting 10 Seconds
    Input Text    xpath://input[@id='email']    Shyam
    Wait Until Element Is Visible    //input[@id='email']    4ms    Email field is not visible after waiting 4 Millis Second
    Textfield Value Should Be    xpath://input[@id='email']    Shyam
    Close Browser

TC 03 Button
    Open Browser    https://www.facebook.com/    chrome
    Maximize Browser Window
    Sleep    2
    Click Button    xpath://input[@value='Log In']

TC 05 ForloopTable
    Open Browser    https://ngendigital.com/practice    chrome
    Sleep    2
    Maximize Browser Window
    Select Frame    id=iframe-015
    Page Should Contain Element

TC 06 ListForloop
    Open Browser    https://ngendigital.com/practice    chrome
    Sleep    2
    Maximize Browser Window
    Select Frame    id=iframe-015
    Page Should Contain List    css:#FromCity
    Sleep    2
    @{cities}    Get List Items    css:#FromCity
    : FOR    ${CITY}    IN    @{cities}
    \    LOG    ${CITY}
    Sleep    4
    List Selection Should Be    css:#FromCity    Toronto
    Sleep    2
    Select From List By Index    css:#FromCity    1
    Sleep    2
    Select From List By Label    css:#FromCity    Chicago
    Sleep    2
    [Teardown]    Close Browser
